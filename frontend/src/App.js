import './css/app.css'
import ElementMaker from './ElementMaker';
import { useState, useEffect } from 'react';
import axios from 'axios';
import { v4 as uuidv4 } from 'uuid';



function App() {
  
  const [edit, setEdit] = useState(false)
  const [showInputEle, setShowInputEle] = useState(false)
  const [text, setText] = useState('')
  const [inputActive, setInputActive] = useState(false)
  const [currentNote, setCurrentNote] = useState(0)
  const [note, setNote] = useState([])
  const [contentText, setContentText] = useState(note[0]?.content)
  const [isLoading, setIsLoading] = useState(true)

  const handleSave = async () => {
    for(let i = 0; i < note.length; i++){
      const res = await axios.post('http://127.0.0.1:8000/notes/create',note[i]);
    }
  }

  const handleAdd = () => {
    setInputActive(true)
  }

  const handleBlur = () => {
    if(text !== ''){
      let temp = note;
      temp.push({
        id:uuidv4(),
        title:text,
        content:''
      })
      setNote(temp)
      setCurrentNote(note.length-1)
    }
    setText('')
    setInputActive(false)
    
  }

  const handleKeyDown = (e) => {
    if(e.key === 'Enter'){
      let temp = note;
      temp.push({
        id:uuidv4(),
        title:text,
        content:''
      })
      setNote(temp)
      setCurrentNote(note.length - 1)
      setText('')
      setInputActive(false)
    }
  }

  const handleChangeTitle = (idx) => {
    setCurrentNote(idx)
  }

  useEffect(() => {
    if(!isLoading){
      setContentText(note[currentNote]?.content)
    }
  },[isLoading])

  useEffect(() => {
    setContentText(note[currentNote]?.content)
  },[currentNote])

  useEffect(() => {
    axios.get('http://127.0.0.1:8000/notes/getnotes')
      .then(response => {
        setNote(response.data)
        setContentText(note[0]?.content)
        setIsLoading(false)
      })
  },[])

  return (
    <div className="App">
      <div className='title'>
        <div>Notes Tracker Everyday</div>
        <div className='save' onClick={handleSave}>Save All Notes</div>
      </div>
      <div className='main'>
        <div className='sidebar'>
          <div className='add' onClick={handleAdd}>Add</div>
          {note.map((data,idx) => {
            if(idx === currentNote){
              return <div className='sidebar-title-current' key={idx}>{data.title}</div>
            }
              return (<div className='sidebar-title'key={idx} onClick={() => handleChangeTitle(idx)}>{data.title}</div>)
          })}
          {
            <input className={inputActive? 'input-yes' : 'input-no'} type="text" placeholder='Insert Note Title' value={text} onChange={(e) => {setText(e.target.value)}} onBlur={handleBlur} onKeyDown={handleKeyDown}></input>
          }
        </div>
        <div className='content'>
          <div className='edit-button' onClick={() => {setShowInputEle(true)}}>edit note  </div>
          {showInputEle ? (
            <input 
            type='text'
            value={contentText}
            onChange={(e) => setContentText(e.target.value)}
            onKeyDown={
              (e) => {
                if(e.key === 'Enter'){
                  let temp = note;
                  temp[currentNote].content = contentText;
                  setNote(temp);
                  setShowInputEle(false);
                }
              }
            }
            onBlur={
              () => {
                let temp = note;
                temp[currentNote].content = contentText;
                setNote(temp);
                setShowInputEle(false)
              }
            }
            />
          ) : 
          <div>{note[currentNote]?.content}</div>
          }
        </div>
      </div>
    </div>
  );
}

export default App;
