After download and unzip the project, in this directory : 

### cd frontend
### yarn 
or
### npm install 

Here, we have finished install our dependencies for frontend
Next, we will install our dependencies for backend 

Go back to r11921108_hw4/ directory :
In r11921108_hw4/ directory :

### cd backend
### pip install poetry
### poetry install

Now, we have finished install our dependencies for backend
Go back to r11921108_hw4/ directory :
### yarn start 
to run the script for running the frontend

The same, from r11921108_hw4/ directory :
### yarn server
to run the script for running the backend

!!!DONT FORGET TO CHANGE THE .env.templates file to .env fle (no need to change the DATABASE_URL since it contains initial data)