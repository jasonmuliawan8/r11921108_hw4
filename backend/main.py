from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from routes.notes import router


app = FastAPI()

origins = ["*"]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(router=router, prefix="/notes")


@app.get("/")
async def get_started():
    return {"message":"Hello World"}
