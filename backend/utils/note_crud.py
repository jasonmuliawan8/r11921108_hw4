from sqlalchemy.orm import Session

from database.models import Notes
from schemas.models import Note


async def note_create(db:Session, note:Note):
    note_found = db.query(Notes).filter_by(id=note.id).all()
    if not note_found :
        db_note = Notes(id=note.id, title=note.title, content=note.content)
        db.add(db_note)
        db.commit()
        db.refresh(db_note)
        return db_note
    update_note = {Notes.title:note.title, Notes.content:note.content}
    db.query(Notes).filter_by(id=note.id).update(update_note)
    db.commit()
    return db.query(Notes).filter_by(id=note.id).one()

async def note_get_all(db:Session):
    return db.query(Notes).all()





