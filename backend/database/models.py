import uuid

from sqlalchemy import Column, String

from database.connection import Base, engine


class Notes(Base):
    __tablename__ = "notes"

    id = Column(String, primary_key=True, index=True)
    title = Column(String)
    content = Column(String)


Base.metadata.create_all(engine)
