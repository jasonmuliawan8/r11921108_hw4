from typing import Optional

from pydantic import BaseModel


class Note(BaseModel):
    id:str
    title:str
    content:str
    class Config:
        orm_mode = True





