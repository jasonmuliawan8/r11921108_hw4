from typing import List

from fastapi import APIRouter, Depends, HTTPException, status
from sqlalchemy.orm import Session

from database.connection import get_db
from schemas.models import Note
from utils.note_crud import (
    note_create,
    note_get_all
)

router = APIRouter(tags=["notes"])


@router.post("/create", status_code=status.HTTP_201_CREATED, response_model=Note)
async def create_note(note: Note, db: Session = Depends(get_db)):
    return await note_create(db=db, note=note)

@router.get("/getnotes", status_code=status.HTTP_200_OK, response_model=List[Note])
async def get_all(db: Session = Depends(get_db)):
    return await note_get_all(db=db)

